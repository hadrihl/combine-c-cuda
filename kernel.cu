#ifndef _KERNEL_H
#define _KERNEL_H

#include <stdio.h>

// device function
__device__ void func2() {
	printf("hello from device:function:func2\n");
}

// device kernel
__global__ void kernel() {
	int idx = threadIdx.x + blockIdx.x * blockDim.x;
	printf("hello from device:kernel of idx = %d\n", idx);

	// call device function:func2
	func2();
}

// start CUDA
extern "C" void cuda_start() {
	printf("Preparing CUDA...\n");
	// var init

	// mem alloc

	// cuda copy device to host

	// kernel launches
	kernel<<< 1, 4 >>>();
	cudaDeviceSynchronize(); // enable temp blocking for printf

	// cuda copy host to device

	// cleanup memory
}

#endif
