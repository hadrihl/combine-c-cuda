CC = gcc
CUDA_CC = nvcc
CC_FLAGS = -Wall -O2
CUDA_FLAGS = -arch=sm_20 -m64

BIN = a.out

all: $(BIN)

main: main.c
	$(CC) $(CC_FLAGS) -c $@ $<

kernel.o: kernel.cu
	$(CUDA_CC) $(CUDA_FLAGS) -c $@ $<

a.out: main.o kernel.o
	$(CUDA_CC) $(CUDA_FLAGS) -o $@ $^

clean:
	$(RM) $(BIN) *.o
