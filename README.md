Combine C with CUDA
===================
Simple use of Makefile to link C with CUDA

Compile the program
-------------------
$ make

Run the program
---------------
$ ./a.out OR cuda-memcheck a.out
