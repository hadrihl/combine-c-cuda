#include <stdio.h>

// host:function
void func1() {
	printf("hello from host:func1\n");
}

int main() {

	// call host function
	func1();

	// call CUDA start
	cuda_start();

	return 0;
}
